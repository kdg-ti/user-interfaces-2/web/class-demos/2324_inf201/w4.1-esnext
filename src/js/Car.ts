export  interface Car    {
  "id": string,
  "license": string,
  "brand": string,
  "model": string,
  "companyCar": boolean,
  "personId": string
}