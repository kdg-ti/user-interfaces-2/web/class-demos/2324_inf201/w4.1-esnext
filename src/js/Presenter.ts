import {Person} from "./Person.js";
import {
  deletePerson,
  getPersons,
  getPersonsWithCars,
  getPersonsWithCarsParallel,
  queryPersons
} from "./restClient.js";
import {Car} from "./Car.js";

const CONTENT = document.getElementById("content")!;
const ERROR_ELEMENT = document.getElementById("error")!;
const PERSONS_BUTTON = document.getElementById("persons-button")!;
const WOMEN_BUTTON = document.getElementById("women-button")!;
const WITH_LICENSES_BUTTON = document.getElementById("persons-with-cars-button")!;
const WITH_LICENSES_PARALLEL_BUTTON = document.getElementById("persons-with-cars-parallel-button")!;

function addEventHandlers() {
  PERSONS_BUTTON.addEventListener("click", getAndShowPersons)
  WOMEN_BUTTON.addEventListener("click", () => filterAndShowPersons("gender=F"))
  WITH_LICENSES_BUTTON.addEventListener("click", getAndShowPersonsWithLicenses)
  WITH_LICENSES_PARALLEL_BUTTON.addEventListener("click", getAndShowPersonsWithLicensesParallel)
}
function showError(error: any) {
  if (!error) {
    return;
  }
  ERROR_ELEMENT.textContent = error.toString();
}

function getAndShowPersons() {
  getPersons()
    .then(person => showPersons(person))
    .catch(error => showError(error));
}

async function filterAndShowPersons(query: string) {
  try {
    const persons = await queryPersons(query)
    showPersons(persons);
  } catch (e) {
    showError(e);
  }
}

async function getAndShowPersonsWithLicenses() {
  try {
    showPersons(await getPersonsWithCars());
  } catch (e) {
    showError(e);
  }

}

async function getAndShowPersonsWithLicensesParallel() {
  try {
    showPersons(await getPersonsWithCarsParallel());
  } catch (e) {
    showError(e);
  }

}


export function init() {
  addEventHandlers();
}


async function removePerson(personId: string = "") {
  if (!personId) {
    return;
  }
  try {
    await deletePerson(personId);
    getAndShowPersons();
  } catch (e) {
    showError(e);
  }
}

function showCar(cars?: Car[]) {
  if (!cars) return "";
  return cars.reduce((acc: String, car) => `${acc}
  <li>${car.license}</li>
  `,
    "<ul>") +"</ul>";
}

export function showPersons(persons: Person[]) {
  //CONTENT.innerHTML = DATA.persons
  CONTENT.innerHTML = persons
    .sort((p1: Person, p2: Person) => (p2?.yearsService ?? -1) - (p1?.yearsService ?? -1))
    .reduce((acc: String, p) => `${acc}
    <tr>
        <td>${p.id}</td>
        <td>${p.firstName} ${p.lastName}</td>
        <td>${p.yearsService}</td>
        <td>${showCar(p.cars)}</td>
        <td><i class="bi bi-trash" role="button" data-person-id="${p.id}"></i></td>
    </tr>
`,
      "<table class='table table-striped'>") + "</table>";
  for (let element of CONTENT.querySelectorAll("[data-person-id]")) {
    element.addEventListener("click", () => removePerson((element as HTMLElement).dataset.personId))
  }
}