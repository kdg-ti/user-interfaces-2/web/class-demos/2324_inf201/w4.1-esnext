import {Car} from "./Car.js";

export   interface Person{
  id?:string // optional to allow for empty id when posting
  firstName: string,
  lastName: string,
  dateOfBirth: string,
  gender: string,
  married?: boolean,
  image?:string,
  yearsService?: number,
  cars?:Car[]
}

// eventueel kan je ook een aparte interface maken die ook de cars bevat
// export interface PersonWithCar extends Person{
//   cars?:Car[]
// }