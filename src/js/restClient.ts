import {Person} from "./Person.js";
import {Car} from "./Car";

const SERVER_URL = "http://localhost:3000";
const PERSONS_URL = SERVER_URL + "/persons";
const CARS_URL = SERVER_URL + "/cars";

export function getPersons(): Promise<Person[]> {
  return fetch(PERSONS_URL)
    .then(response => {
      if (response.ok) {
        return response.json();
      } else {
        throw new Error(`Fetch persons failed with status ${response.status}: ${response.statusText}`)
      }
    })
}

export async function queryPersons(query: string): Promise<Person[]> {
  const response = await fetch(`${PERSONS_URL}?${query}`);
  if (response.ok) {
    return response.json();
  } else {
    throw new Error(`Fetch persons failed with status ${response.status}: ${response.statusText}`)
  }
}

async function getCarsByPerson(id?: string):Promise<Car[]> {
  if(!id){ throw new Error ("Cannot lookup cars without ID")}
  const response = await fetch(`${CARS_URL}/?personId=${id}`);
  if (!response.ok) {
    throw new Error(`Fetch cars failed with status ${response.status}: ${response.statusText}`)
  }
  return response.json();
}

export async function getPersonsWithCars(): Promise<Person[]> {
  const response = await fetch(PERSONS_URL);
  if (!response.ok) {
    throw new Error(`Fetch persons failed with status ${response.status}: ${response.statusText}`)
  }
  const persons:Person[] = await response.json();
  const start = performance.now();

  for (let  person of persons){
    person.cars = await getCarsByPerson(person.id);
  }
  console.log("retrieve cars loop:"  + (performance.now()-start))

  return persons;
}

export async function getPersonsWithCarsParallel(): Promise<Person[]> {
  const response = await fetch(PERSONS_URL);
  if (!response.ok) {
    throw new Error(`Fetch persons failed with status ${response.status}: ${response.statusText}`)
  }
  const persons:Person[] = await response.json();
  const start = performance.now();
  const personsWithCars :Person[] = await Promise.all(persons.map(async(person) => {
    person.cars= await getCarsByPerson(person.id);
    return person;
  }))
  console.log("retrieve cars //:"  + (performance.now()-start))
  return personsWithCars;
}

export async function deletePerson(personId:string){
  const response = await fetch(`${PERSONS_URL}/${personId}`,{method:"DELETE"});
  if (!response.ok) {
    throw new Error(`Fetch persons failed with status ${response.status}: ${response.statusText}`)
  }
}