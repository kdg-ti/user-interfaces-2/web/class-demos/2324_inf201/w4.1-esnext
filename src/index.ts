import "bootstrap"; // importeer bootstrap JavaScript code
import "bootstrap/dist/css/bootstrap.css"; // importeer bootstrap CSS code
import "bootstrap-icons/font/bootstrap-icons.css"; // importeer Bootstrap icons code
import "./css/style.css";
import {init} from "./js/Presenter.js";


init();
