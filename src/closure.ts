let objecten = "crows";

function showMessage(number:number) {
  console.log(number + " " + objecten);
}

showMessage(5);

function makeCounter() {
  let count = 2;

  return function () {
    return count++;
  };
}

const counter = makeCounter();
const otherCounter = makeCounter();

showMessage(counter()); // 2 crows
showMessage(counter()); // 3 crows
showMessage(counter()); // 4 crows
showMessage(otherCounter()); // 4 crows


// multiplier
console.log("multipliers")
function getMultiplier(factor : number){

  // function doIt( nr:number){
  //  return  nr * factor;
  // }
  //return doIt;
  return (nr:number) => nr*factor;
}

const twice = getMultiplier(2);
const fiveFold = getMultiplier(5);
console.log(twice(5));
console.log(fiveFold(10));

// spread en rest
console.log(Math.max(45, 78, 556));

let  a = [2,3,4];
console.log([ 1, a, 5 ]); // [ 1, [ 2, 3, 4 ], 5 ]
console.log([ 1, ...a, 5 ]); // [ 1, 2, 3, 4 , 5 ]

let b = [...a];
b[2]=666;
console.log(a);